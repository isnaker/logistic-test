$(document).ready(function () {
  new WOW().init();
  $('.mobile-menu-button').on('click', function(e){
   $('.mobile-line').addClass('active-mobile');
   $('.overlay').addClass('mobile-overlay');
   $('.active-mobile-button').addClass('mobile-open');
   $('body').addClass('body-mobile-fix');
 });

  $('.active-mobile-button').on('click', function(e){
   $('.mobile-line').removeClass('active-mobile');
   $('.overlay').removeClass('mobile-overlay');
   $('.active-mobile-button').removeClass('mobile-open');
      $('body').removeClass('body-mobile-fix');
 });


  $('.whoweare__left-bottom li').on('click', function(){
   $(this).toggleClass('active');
 });
  var en = $('[value="en"]');
  var ru = $('[value="ru"]');
  if ( en.attr('selected') )
  {
    $('.logo-eng').removeClass('logo-hover');
    $('.logo-rus').addClass('logo-hover');
    $('.benefits-eng').removeClass('benefits-hover');
    $('.benefits-rus').addClass('benefits-hover');
  }
  else
  {
    $('.logo-rus').removeClass('logo-hover');
    $('.logo-eng').addClass('logo-hover');
    $('.benefits-rus').removeClass('benefits-hover');
    $('.benefits-eng').addClass('benefits-hover');
  }
  $('.owl-carousel').owlCarousel({
    responsive:{
      1600:{
        items:1
      },
      1000:{
        items:1
      },
      500:{
        items:1
      },
      0:{
        items:1
      }
    },
    autoplay:true,
    autoplayTimeout:90000,
    loop:true
  });
  $(window).scroll(function(){
    if ($(this).scrollTop() > 50) {
     $('.pages-header-bottom').addClass('fixed-menu');
     $('.header-nav').addClass('main-fixed-menu');
     $('.active-mobile').addClass('active-mobile-fix');
     $('body').addClass('body-pad');
   }
   else{
    $('.pages-header-bottom').removeClass('fixed-menu');
    $('.header-nav').removeClass('main-fixed-menu');
    $('.active-mobile').removeClass('active-mobile-fix');
    $('body').removeClass('body-pad');
  }
});
  $(function($){
   $("#Phone,#Phone2,#Phone3,#Phone4").mask("(999) 999-99-99");
   $("#Gabarity").mask("99x99x99");
   $("#code").mask("9999999999");
 });
  $('.vertical-menu li').find('a').each( function(){
    if($(this).attr('href')==$(location).attr('href')){
      $(this).parent().toggleClass('current');  
    }
  });

  $('.nav-button.form-click a').on('click', function(e){
   $('.modalwindow, .modalwindow__inner').addClass('active');
 });
  $('.modalwindow-close').on('click', function(e){
   $('.modalwindow, .modalwindow__inner').removeClass('active');
 });

  
  $('.slick-class').slick({
    centerMode: true,
    centerPadding: '0px',
    slidesToShow: 3,
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        arrows: false,
      }
    },

    {
      breakpoint: 768,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '00px',
        slidesToShow: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    }
    ]
  });

});
